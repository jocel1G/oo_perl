#/usr/bin/perl
use strict;
use warnings;

package File_In_file;

use Scalar::Util 'blessed';
use Data::Dumper;

# Constructor
# could be named load; cf. page http://perldoc.perl.org/perlobj.html#An-Object-is-Simply-a-Data-Structure
# The modern convention for OO modules is to always use new as the name for the constructor...
sub new
{
    # De page http://woufeil.developpez.com/tutoriels/perl/poo/
    my ($classe, $path, $content, $last_mod_time) = @_;    #on passe les données au constructeur
    my $this = {
        "path" => $path,
        "content" => $content,
        "last_mod_time" => $last_mod_time,
    };        #référence anonyme vers une table de hachage vide
    
    bless( $this, $classe );    #lie la référence à la classe
    return $this;      #on retourne la référence consacrée
}

sub print_info
{
    my $self = shift;
    print $self->{"path"} . ".\n";

    print "This file is at ", $self->{"path"}, ".\n"; 
}

# package Pack2;
# use File;

my $hostname = File_In_file->new('/etc/hostname', "foo\n", 1304974868);

#my $hostname = new File('/etc/hostname', "foo\n", 1304974868);

print Dumper($hostname);


# print blessed($hash);
print blessed($hostname) . ".\n";

$hostname->print_info;