use strict;
use warnings;

package File;

use Data::Dumper;

# Constructor
# could be named load; cf. page http://perldoc.perl.org/perlobj.html#An-Object-is-Simply-a-Data-Structure
# The modern convention for OO modules is to always use new as the name for the constructor...
sub new
{
    my $class = shift;

    my $self = {
        path => shift,
        content => shift,
        last_mod_time => shift,
    };

    print "Path = " . $self->{path} . "\n.";
    print "Content = " . $self->{content} . "\n.";
    print "last_mod_time = " . $self->{last_mod_time} . "\n.";
    bless $self, $class;

    

    # return bless {}, $class;
    return $self;
}

sub print_info
{
    my $self = shift;

    print Dumper($self);
    print $self->{path};

    #print "This file is at ", $self->path, "\n";

    #print "kkkkkk\n";
}
1;