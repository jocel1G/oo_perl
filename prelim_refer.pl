#/usr/bin/perl
use strict;
use warnings;

# Hard references
my $foo = 'Bill';
my $fooref = \$foo;

print $$fooref . "\n";

# Anonymous arrays
my $array = ['Bill', 'Ben', 'Mary'];
print $$array[1] . "\n";

my @arrayarray = (1, 2, ([3, 4, 5]));
# From http://perldoc.perl.org/perlref.html, Making References
my $arrayref = [1, 2, ['a', 'b', 'c']];

# Cf Use Rule 2 of page http://perldoc.perl.org/perlreftut.html
# Either
# print $arrayref->[2][1] . "\n";
# Or
print $$arrayref[2][1] . "\n";

# Arrow Rule, page http://perldoc.perl.org/perlreftut.html
# Either
# print $arrayarray[2]->[0];
# Or
print $arrayarray[2][0] . "\n";

# Anonymous hashes
my $hash = { 'Man' => 'Bill',
          'Woman' => 'Mary',
          'Dog' => 'Ben'
};
# Either
# print $$hash{'Woman'};
# Or 
print $hash->{'Woman'} . "\n";

# Anonymous subroutine
# Either the following block
# From page http://perldoc.perl.org/perlref.html, Making References 4
# my $coderef = sub { print "Boink!\n" };
# &$coderef;
# Or
sub foo
{
    print "foo\n";
};
my $foosub = \&foo;
&$foosub();

my $glob = \*STDOUT;    # Create reference to typeglob
print $glob "Hello World!\n";