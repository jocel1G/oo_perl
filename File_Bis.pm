#/usr/bin/perl
use strict;
use warnings;

package File_Bis;

use Data::Dumper;

# Constructor
# could be named load; cf. page http://perldoc.perl.org/perlobj.html#An-Object-is-Simply-a-Data-Structure
# The modern convention for OO modules is to always use new as the name for the constructor...
sub new
{
    # De page http://woufeil.developpez.com/tutoriels/perl/poo/
    my ($classe, $path, $content, $last_mod_time) = @_;    #la fonction reçoit comme premier paramètre le nom de la classe
    my $this = {
        "path" => $path,
        "content" => $content,
        "last_mod_time" => $last_mod_time,
    };
    
    bless( $this, $classe );    #lie la référence à la classe
    return $this;      #on retourne la référence consacrée

}

sub print_info
{
    my ($self) = @_;

    #print Dumper($self) . "\n.";
    print $self->{path} . ".\n";

    print "This file is at ", $self->{"path"}, ".\n";

    #print "kkkkkk\n";
}
1;